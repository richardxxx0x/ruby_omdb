class CreateMovies < ActiveRecord::Migration
  def change
    create_table :movies do |t|
      t.string :title
      t.string :director
      t.integer :year
      t.string :plot
      t.integer :oscars

      t.timestamps null: false
    end
  end
end
